#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include<mpi.h>
using namespace std; 

//bool RANDOM = false; 
bool RANDOM = true; 

void buildMatrix(double ** &a,int n, int m){
  //srand (time(NULL));
  srand (1);
  a = new double *[n]; 
  a[0] = new double [m*n];

  for (int i = 1; i < n; i++){
    a[i] = a[i - 1] + m;
  }

  for (int i = 0 ; i < n ; i++) {
    for (int j = 0 ; j < m ; j++) {  
      if (RANDOM){
        a[i][j] = double((rand()%100 +1));
      } else {
        a[i][j] = i * j + 1 ;
      }
    }
  }
}
void buildVector(double * &b , int n, bool initZero ){
  b = new double [n];
  for (int i = 0 ; i < n ; i++) {
    if (initZero){
      b[i] = 0;
    } else {
      if (RANDOM){
        b[i] = double((rand()%100 +1)); ;
      } else {
        b[i] = i + 1 ;
      }
    }
    //a[i][j] = double((rand()%100 +1));
  }
}

void printVector(double * &b, int n){
  for (int i = 0 ; i < n ; i++) {
    printf("%2.3f ", b[i]);
  }
  printf("\n");
}

void printMatix(double ** &A, int row, int col){
  for (int i = 0 ; i < row ; i++) {
    for (int j = 0 ; j < col ; j++) {
       printf("%2.3f ", A[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

void matrixMult(double ** &A,double * &X,double * &B, int rows,int cols, int numProcesses, int myPid){
  // current col --> myPid % cols --> index for vector b 
  //                              --> index for vector a 
  //                              --> 
  // current row --> myPid / cols --> 
  //                              --> each process gets their current row based on myPid
  //                              --> SOULTION DESTINATION ROW 
  double sum = 0; 
  double temp = 0; 
  double aA = 0; 
  double xX = 0; 
  MPI_Status status;
    if (myPid == 0) {
      // how to send each process a copy of its row ? 
      // I only need to send one value not the whole row 
      for (int i = 0 ; i < rows * cols ; i++) {
         //a = A[ i / cols ]; --> whole row
         if (i != 0) {
           aA =  A[ i / cols ][ i % cols ];

           //MPI_Send(&A[i/cols][i%cols], 1,MPI_DOUBLE,i,0,MPI_COMM_WORLD);
           //MPI_Send(&B[i%cols], 1,MPI_DOUBLE,i,1,MPI_COMM_WORLD);

           MPI_Send(&A[i/cols][i%cols], 1,MPI_DOUBLE,i,0,MPI_COMM_WORLD);
           MPI_Send(&X[i%cols], 1,MPI_DOUBLE,i,1,MPI_COMM_WORLD);
         }
      }
    // Assign pid a set of A and B values to do a calculation. 
    aA = A[0][0];
    xX = X[0];

    } else {
      MPI_Recv(&aA, 1,MPI_DOUBLE,0,0,MPI_COMM_WORLD, &status);
      MPI_Recv(&xX, 1,MPI_DOUBLE,0,1,MPI_COMM_WORLD, &status);
    }

    // sum is the value of A [i] * b [j]
    // Each process calculates it own value
    // and sends this back to the main process
    // this main process then sums on the rows 
    // B[myPid%cols] = A[myPid/cols][myPid%cols] * X[myPid%cols] 

    sum = aA * xX; 
 
    // send this value using the pid and use the 
    // location in the myPid % cols to sum back in the orginal process?
    if (myPid != 0 ){ 
      MPI_Send(&sum,1,MPI_DOUBLE,0,myPid,MPI_COMM_WORLD);
    }
    else {
        //X[0] += sum;
        B[0] += sum;
        printf("\n%f\n", sum);
      for (int i = 1 ; i < rows * cols ; i++) {
        MPI_Recv(&temp,1,MPI_DOUBLE,i,i,MPI_COMM_WORLD, &status); 
        //X[i/cols] += temp;
        B[i/cols] += temp;
      }
    }
}


bool helper(int argc, char* argv[], int &rows, int & cols, int & printit){
  if(argc < 3) {
    printf( "Usage:\n");
    printf( "%s {matrix rows} {matrix cols} [0|1]\n\n\n", argv[0]);
    printf( "mpirun -print-rank-map --hostfile hosts -np {matrix rows} -ppn 1 %s {matrix rows} {matrix cols} [print]",argv[0]);
    return false;
  }else{
    rows = atoi(argv[1]);
    cols = atoi(argv[2]);
    
    if (rows <=0 || cols <= 0 ) {
      printf( "Matrix row size and matrix col size \n");
      printf( "must be valid intergers > 0 \n\n\n");
    }
    
    if ( argc == 4){
    //printf("%d\n",atoi(argv[3]));
    //printf("%d\n",cols);
    //printf("%d\n",);
      printit = atoi(argv[3]);
      if ( printit != 1 ) {
        printit == 0;
      }
    } else {
      printit == 0;
    }
      
    return true;
  } 
}


int main(int argc, char* argv[]) {
  // rowM X (colM and rowV) X colV --> (rowM and colV)
  // 
  // where colM == rowV
  //
  // 000000   0
  // 000000 x 0  --> 0
  // 000000   0      0
  //          0      0
  //          0 
  int row = 0;
  int col = 0;
  double ** A; 
  double * b; 
  double * x; 

  int printResults;
  int numProcesses = 4 ;
  int Pid;
  double runtime;
  bool test = false; 

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numProcesses);
  MPI_Comm_rank(MPI_COMM_WORLD, &Pid);
  

  if (Pid == 0) {
  test = helper(argc, argv, row, col, printResults);
  if (test == false) {
    MPI_Finalize();
    return 1;
  }
    printf("Matrix dim. => %d X %d \n", row, col );
    printf("Vector dim . => %d X 1 \n", col);
    printf("Solution dim . => %d X 1 \n", row);
     
    buildMatrix(A, row, col);
    buildVector(b, row, true);
    buildVector(x, col, false);
    runtime = MPI_Wtime();
  } 
  // all processes execute this 
  matrixMult(A, x, b, row, col, numProcesses, Pid);
  
  if (Pid == 0) {  
    runtime = MPI_Wtime() - runtime;
    //printVector(x, row);
    if (printResults == 1){
      printf("Matrix:\n");
      printMatix(A, row, col);
      printf("Vector: \n");
      printVector(x, col);
      printf("Solution: \n");
      printVector(b, row);
    }
      printf("Runtime: %2.4f\n", runtime);
  } 
  MPI_Finalize();
}

  // void matrixMult(double ** &A,double * &X,double * &B, int rows,int cols, int numProcesses, int myPid){
  //////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////
  //}
  //  double * a = new double [rows];
  //  double * b = new double [rows];
  //  //double * x = new double [rows]; 
  //  double sum = 0; 
  //  MPI_Status status;

  //  if (myPid == 0) {
  //    b = B;
  //    for (int i = 0 ; i < rows ; i++) {
  //       a = A[i];
  //       //x[i] = 0;
  //       if (i != 0) {
  //         MPI_Send(a, cols,MPI_DOUBLE,i,0,MPI_COMM_WORLD);
  //         MPI_Send(b, cols,MPI_DOUBLE,i,1,MPI_COMM_WORLD);
  //         //MPI_Send(x, cols,MPI_DOUBLE,i,2,MPI_COMM_WORLD);
  //       }
  //    }
  //  } else {
  //    MPI_Recv(a, cols,MPI_DOUBLE,0,0,MPI_COMM_WORLD, &status);
  //    MPI_Recv(b, cols,MPI_DOUBLE,0,1,MPI_COMM_WORLD, &status);
  //    //MPI_Recv(x, cols,MPI_DOUBLE,0,2,MPI_COMM_WORLD, &status);
  //    //printf("here with pid %d with\n", myPid); 
  //  }

  //  if (myPid == 0) {
  //    a = A[0];
  //  }


  //  MPI_Barrier(MPI_COMM_WORLD);
  //  //for (int i = 0 ; i < rows ; i++) {
  //  for (int i = 0 ; i < cols ; i++) {
  //    //x[i] +=(a[i] * b[i]);
  //    sum +=(a[i] * b[i]);
  //    //printf("%2.3f ", b[i]); 
  //    //printf("%2.3f ", a[i]); 
  //  }
  //  printf("\nsum %2.3f ,at %d\n", sum, myPid);


  //  //x[myPid] = sum ;
  //  MPI_Barrier(MPI_COMM_WORLD);

  //  if (myPid != 0){
  //    //printf("send HERE\n");
  //    //MPI_Send(&x,rows,MPI_DOUBLE,0,myPid,MPI_COMM_WORLD);
  //    //MPI_Send(&x,1,MPI_DOUBLE,0,myPid,MPI_COMM_WORLD);
  //    MPI_Send(&sum,1,MPI_DOUBLE,0,myPid,MPI_COMM_WORLD);
  //  }

  //  MPI_Barrier(MPI_COMM_WORLD);

  //  //2. send back to pid 0 
  //  if (myPid == 0 ) {

  //    X[0] = sum;
  //    //for (int i = 1; i < rows ; i++) {
  //    //printf("$$$$$$$$$$$$$$$$\n");
  //    
  //    for (int i = 1 ; i < rows; i++) {
  //    //for (int i = 1 ; i < cols; i++) {
  //      //MPI_Recv(&x, rows,MPI_DOUBLE,i,i,MPI_COMM_WORLD, &status);
  //      //MPI_Recv(&x[i],1,MPI_DOUBLE,i,i,MPI_COMM_WORLD, &status);
  //      MPI_Recv(&X[i],1,MPI_DOUBLE,i,i,MPI_COMM_WORLD, &status);
  //      //printf("sum -->  %2.3f ,at %d\n", x[i], i);
  //    }
  //      //x[i] = sum;
  //    //}
  //  }

  //  if (myPid == 0)  {  
  //    delete[] a;
  //    delete[] b;
  //  }
//}
