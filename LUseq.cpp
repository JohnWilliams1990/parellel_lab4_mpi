// John Williams 
// Parallel and Distributed systems
//

#include <iostream>
#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<time.h>
#include <sys/time.h>
using namespace std; 

// struct matrix_t{
//   double **A;
//   double *b;
//   double *x;
//   int dimention; 
// };

double **alloc_2d_double(int n);
void eyes( double ** matrix, int dim);
//void printMat( struct matrix_t* , int mat );
void printArr(double **M , int mat );
void multiply(double** C, double **L ,double **R, int dim);
void cleanupMatrix(double ** arr, int dim);
//void read_in_file(FILE* file, matrix_t * matPtr);
bool read_in_file(FILE* file, matrix_t * matPtr);

int main(int argc, char* argv[]) {
  





}
///////////////////////////////////////////////////////////////////

double **alloc_2d_double(int n) {
  double *data = (double *)malloc(n * n * sizeof(double));
  double **array= (double **)malloc(n*sizeof(double*));
  for (int i=0; i<n; i++)
    array[i] = &(data[n*i]);    
  return array;
}

void eyes( double ** matrix, int dim){
  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
      if (i == j){
        matrix[i][j] = 1;
      }
      else {
        matrix[i][j] = 0;
      }
    }
  }
}

void multiply(double** ans, double **L ,double **R, int dim){
  double ** C = alloc_2d_double(dim);

  for(int i = 0; i < dim; i++){
    for(int j = 0; j < dim; j++){
      C[i][j] = 0.0;
      for(int k = 0; k < dim; k++){      
        C[i][j] = C[i][j] + L[i][k] * R[k][j];
      }
    }
    ans [i] = C[i];
  }
  free(C);
  // assignment to ans that is passed in;
  // unsure of how to do assignment with out creating a new array


  // for(int i = 0; i < dim; i++){
  //   ans [i] = C[i];
  // }
}

void cleanupMatrix(double ** arr, int dim){
  for (int i = 0; i < dim; i++){
      double* currentRow = arr[i];
      free(currentRow);
  }
}

// // 0 is array. 1 is lower. 2 is upper
// void printMat( matrix_t* matPtr , int mat ){
//   for(int i = 0; i <  matPtr->dimention; i++){
//     for(int j = 0; j <  matPtr->dimention; j++){
//       if (mat == 0){
//         printf("%1.3f ", matPtr->array[i][j]);
//       } else if (mat == 1){
//         printf("%1.3f ", matPtr->lower[i][j]);
//       } else if (mat == 2){
//         printf("%1.3f ", matPtr->upper[i][j]);
//       }
//     }
//     printf("\n");
//   }
//   printf("\n");
// }
// 
void printArr(double **M , int mat ){
  // 0 is array. 1 is lower. 2 is upper
  for(int i = 0; i <  mat; i++){
    for(int j = 0; j <  mat; j++){
        printf("%1.3f ", M[i][j]);
    }
    printf("\n");
  }
  printf("\n");
}

// bool read_in_file(FILE* file, matrix_t * matPtr){
//   int input = 0;
//   bool end = false;
//   fscanf (file, "%d", &input);    
//   //if( fscanf (file, "%d", &input) == EOF){end = true; printf("HERE"); return end; };
//   //printf("val: %d\n", input); 
//   if (input <= 0){ return true; /*exit(0);*/}
//   matPtr->dimention = input;
//   matPtr->array = alloc_2d_double( matPtr->dimention);
//   matPtr->lower = alloc_2d_double( matPtr->dimention);
//   matPtr->upper = alloc_2d_double( matPtr->dimention);
//   eyes(matPtr->lower,  matPtr->dimention);
//   for(int i = 0; i <  matPtr->dimention; i++){
//     for(int j = 0; j <  matPtr->dimention;  j++){
//      if( fscanf (file, "%d", &input) == EOF){end = true;};
//       matPtr->array[i][j] = input;
//       matPtr->upper[i][j] = input;
//     }
//   }
//    
//   matPtr->b = (double *)malloc( matPtr->dimention*sizeof(double*));
//   matPtr->x = (double *)malloc( matPtr->dimention*sizeof(double*));
//   printf("B: ");
//   for(int i = 0; i <  matPtr->dimention; i++){
//      if( fscanf (file, "%d", &input) == EOF){end = true;};
//      matPtr->b[i] = input;
//      printf("%f ",matPtr->b[i]);
//   }
//      printf("\n\n ");
//   // printMat(matPtr, 0);
//   // printMat(matPtr, 1);
//   // printMat(matPtr, 2);
//   return end;
// }

