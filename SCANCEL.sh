#!/bin/bash

for i in `squeue | grep john.2 | awk '{print $1}'` ; do 
  printf "Canceling $i \n"
  scancel $i ; 
done
