#!/bin/bash

function maker {
  mpif90 fge-mpi1.f90 -o fge-mpi1
  mpif90 fge-mpi2.f90 -o fge-mpi2
  mpicxx cge-mpi1.cpp -o cge-mpi1
  mpicxx cge-mpi2.cpp -o cge-mpi2
  mpicxx MM_Williams-John.cpp -o solution
  #mpig++ -O `pwd`/cge-mpi1.cpp -o `pwd`/cge-mpi1
  #mpig++ -O `pwd`/cge-mpi2.cpp -o `pwd`/cge-mpi2
  #mpif90 -O `pwd`/fge-mpi1.f90 -o  `pwd`fge-mpi1
  #mpif90 -O `pwd`/fge-mpi2.f90 -o  `pwd`fge-mpi2
}
function makeclean {
  rm -f fge-mpi1 fge-mpi2 cge-mpi1 cge-mpi2 solution
}

function printit {
  echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  # cat slurm_output.* | egrep "Gaus|Matri"
  # cat slurm_output.* | egrep "Gaus|Matri" | grep secon | awk '{print $5}'
  for i in `cat slurm_output.* | egrep "Gaus|Matri" | grep secon | awk '{print $5}'` ; do 
    printf "$i "
  done
  printf "\n"
  echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
  echo
}

function alarm {
  chime=6
  while [ ! -z "$(./nodeManager.sh -v )" ]; do 
    continue
  done
  for i in `seq 1 $chime ` ; do 
    echo -ne "\a"
    sleep 2
  done 
}


if [ -z "$1" ] ; then 
  printf "\n"
  printf "\tUsage $0 [ -c | -m | -r | -R {fge-mpi1|fge-mpi2|cge-mpi1|cge-mpi2} ]\n\n"
  printf "\t$0 -b run MM_Williams-John.cpp\n"
  printf "\t$0 -c make clean\n"
  printf "\t$0 -r read results from run\n"
  printf "\t$0 -m make all files\n"
  printf "\t$0 -R {fge-mpi1 | fge-mpi2 | cge-mpi1 | cge-mpi2} run individual program with set of threads \n\n"
  printf "\n\n"
  exit 1 
elif [ "$1" = "-c" ]; then 
  makeclean
  rm -rf slurm_*
  exit 0 
elif [ "$1" = "-m" ]; then 
  maker
  exit 0 
elif [ "$1" = "-r" ]; then 
  printit
  exit 0 
fi


size=9984
#ntasks=(2 48 96 192 312 )
#nodesPerTask=(2 24 24 24  24 )

ntasks=( 312 192 96 48 2 )
nodesPerTask=( 24 24 24 24 2 )
# $1 --> flag
# $2 --> program with ./
# $3 --> ntasks
# $4 --> nodes per task
# $5 --> size

if [ "$1" = "-b" ]; then 
  ./run.sh -c 
  ./run.sh -m 
  numRows=10
  numCols=3

  tasks=( 30 63 130 240 1000 )
  rows=( 6 9 13 24 100 )
  cols=( 5 7 10 10 10  )

  numberProcesses=10

  # COLS
  #numberTasks=30
  numberTasks=63
  numberTasks=130
  # ROWS

  
  #for i in `seq 0 4`; do
  for i in `seq 0 4`; do
   
     mpirun -print-rank-map --hostfile hosts -np ${tasks[i]} -ppn $numberProcesses `pwd`/solution ${rows[i]} ${cols[i]} 1
    #mpirun -print-rank-map --hostfile hosts -np ${tasks[i]} -ppn $numberProcesses `pwd`/solution ${cols[i]} ${rows[i]} 1
  done

  exit 0 
fi


if [ "$1" = "-R" ] && [ ! -z "$2" ]; then 
  if [ "$2" != "fge-mpi1" ] && [ "$2" != "fge-mpi2" ] && [ "$2" != "cge-mpi1" ] && [ "$2" != "cge-mpi2" ]; then 
    printf "\n\tusage $0 -R [ fge-mpi1 | fge-mpi2 | cge-mpi1 | cge-mpi2 ] \n\n"
    exit 1
  else 
    $0 -c
    maker
    for i in `seq 0 4`; do
      echo "mpirun -print-rank-map --hostfile hosts -np ${ntasks[i]} -ppn ${nodesPerTask[i]} `pwd`/$2 $size "
      mpirun -print-rank-map --hostfile hosts -np ${ntasks[i]} -ppn ${nodesPerTask[i]} `pwd`/$2 $size

      #echo ${ntasks[i]} 
      #echo ${nodesPerTask[i]} 
    done

    # alarm &  
  fi
else 
  printf "\n\tusage $0 -R [ fge-mpi1 | fge-mpi2 | cge-mpi1 | cge-mpi2 ]\n\n"
  exit 1
fi
