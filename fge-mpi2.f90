!-----------------------------------------------------------
! Parallel Gaussian Elimination - Fortran MPI Version 2
!-----------------------------------------------------------
!  Some features:
!  + For simplicity, program requires matrix size 
!    must be multiple of processes. 
!  + Data partition model of each process is block
!  + Data communication: Scatter / Gather / Broadcast
!  Programming by Gita Alaghband, Lan Vu 
!  Update in 8/17/2010
! ----------------------------------------------------------
! Get user input of matrix dimension and printing option
! -----------------------------------------------------------
      SUBROUTINE GetUserInput(n,isPrint,isOK,numProcesses, myProcessID)
      INTEGER :: argc
      CHARACTER(10) :: argv
      INTEGER , INTENT(IN):: numProcesses, myProcessID
      INTEGER , INTENT(OUT):: n,isPrint
      LOGICAL , INTENT(OUT):: isOK
      
      isOK = .TRUE.
      argc = iargc() 
      IF (argc < 1) THEN
           IF (myProcessID == 0) THEN
			   PRINT *,"Arguments:<X> [<Y>]"        
			   PRINT *,"X : Matrix size [X x X]"              
			   PRINT *,"Y = 1: print the input/output matrix if X < 10"         
			   PRINT *,"Y <> 1 or missing: does not print the input/output matrix"         
           ENDIF 			
           isOK = .FALSE.     
      ELSE
           ! get matrix size argument     
           call getarg(1, argv)
           read (argv,*) n
           IF (n <= 0 ) THEN
                IF (myProcessID == 0) PRINT *,"Matrix size must be larger than 0"         
                isOK = .FALSE.     
           ENDIF
           
           ! check if matrix size is multiple of the number of processes
		   IF ( MOD(n,numProcesses ) /= 0 ) THEN
		        IF (myProcessID == 0) PRINT *,"Matrix size must be multiple of the number of processes"
                isOK = .FALSE.     
           ENDIF
           
                                 			
           ! is print the input/output matrix argument
           isPrint = 0
           IF (argc >= 2) THEN
                call getarg(2, argv)
                read (argv,*) isPrint            
                IF (isPrint == 1 .AND. n <= 9) THEN
                     isPrint = 1
                ELSE
                     isPrint = 0
                ENDIF
           ENDIF     
      ENDIF  
      RETURN
      END SUBROUTINE GetUserInput
!----------------------------------------------------------------
!  Initialize the value of matrix a[n x n] for Gaussian elimination  
!----------------------------------------------------------------
      SUBROUTINE InitializeMatrix(a, n)
      INTEGER :: i,j
      REAL  :: a(n,n)
      DO j = 1,n 	  		
           DO i = 1,n
                IF (i == j) THEN
                     a(i,j) = i*i/2.	
                ELSE
                     a(i,j) = (i+j)/2.
                ENDIF
           ENDDO
      ENDDO
      RETURN
      END SUBROUTINE InitializeMatrix
!----------------------------------------------------------------
!  Print matrix  
!----------------------------------------------------------------
      SUBROUTINE PrintMatrix(a, n)
      INTEGER :: i,j
      REAL :: a(n,n)
      DO i = 1,n 	  		
           WRITE (*,'(a,i1,a,$)') "Row ",i,":"
           DO j = 1,n 	  		
                WRITE (*,'(F8.2,$)')  a(i,j)
           ENDDO 

           ! go to new line
           PRINT *,""
      ENDDO
      RETURN
      END SUBROUTINE PrintMatrix
!----------------------------------------------------------------
!  Compute the Gaussian Elimination for matrix a[n x n]
!----------------------------------------------------------------
      SUBROUTINE ComputeGaussianElimination(a,n,isOK,numProcesses,myProcessID)                      
      USE mpi
      REAL :: pivot,max,temp
      INTEGER :: sCol,nCols,indmax,i,j,k,lk,h,ierr,request
      INTEGER :: n,numProcesses,myProcessID
      REAL :: a(n,n)
      REAL :: tmp(n)
      REAL , ALLOCATABLE, DIMENSION(:,:) :: b
      LOGICAL, INTENT(OUT) :: isOK
      INTEGER :: status(MPI_STATUS_SIZE) 

      isOK = .TRUE.
      nCols = n/numProcesses

      ALLOCATE(b(n,nCols))

      ! process 0 send the data to compute nodes
      CALL MPI_Scatter(a,n*nCols,MPI_REAL,b,n*nCols,MPI_REAL,0,MPI_COMM_WORLD,ierr)

      ! Perform colmunwise elimination
      DO k = 1,n-1

           max = 0.0
           indmax = k

           ! ID of master process
           master = (k-1)/nCols;
           
           ! local k
           lk = MOD(k-1,nCols)+1

           ! Only master process find the pivot column
           ! Then broadcast it to all other processes

           IF (myProcessID == master) THEN
                DO i = k,n
                    temp = abs(b(i,lk))
                    IF (temp > max) THEN
                         max = temp
                         indmax = i
                    ENDIF
                ENDDO
           ENDIF

           ! Master process broadcast the max,indmax to all processes
           CALL MPI_Bcast(max,1,MPI_REAL,master,MPI_COMM_WORLD,ierr)
           CALL MPI_Bcast(indmax,1,MPI_INTEGER,master,MPI_COMM_WORLD,ierr)

           ! If matrix is singular set the flag & quit

     	   IF (max == 0) isOK = .FALSE.
     	   IF (isOK .eqv. .FALSE.) GOTO 100

           ! Swap columns if necessary
           IF (indmax /= k .AND. myProcessID >= master) THEN
		        IF (myProcessID > master) THEN
		             h = 1
		        ELSE
		             h = lk
                ENDIF

                DO j = h ,nCols
                     temp = b(indmax,j)
                     b(indmax,j) = b(k,j)
                     b(k,j) = temp
                ENDDO
           ENDIF

           ! Master 
           IF (myProcessID == master) THEN
               pivot = -1.0/b(k,lk)
               i = k + 1
               tmp(i:n)= pivot*b(i:n,lk)
		   ENDIF       

           CALL MPI_Bcast(tmp(k + 1) ,n - k,MPI_REAL,master,MPI_COMM_WORLD,ierr)

           ! Perform columnwise reductions
           IF (myProcessID >= master) THEN
		        IF (myProcessID > master) THEN
		             h = 1
		        ELSE
		             h = lk
                ENDIF
                i = k + 1
                DO j = h , nCols
	                 b(i:n,j) = b(i:n,j) + tmp(i:n)*b(k,j)
	            ENDDO
	            
           ENDIF
      ENDDO

      ! process 0 collects results from the worker processes
      CALL MPI_Gather(b,n*nCols,MPI_REAL,a,n*nCols,MPI_REAL,0,MPI_COMM_WORLD,ierr)

      DEALLOCATE(b)
      
100   RETURN
      END SUBROUTINE ComputeGaussianElimination
!-----------------------------------------------------------
!  Main program
!-----------------------------------------------------------
      PROGRAM GaussianElimination
      USE mpi
      IMPLICIT NONE
      REAL , ALLOCATABLE, DIMENSION(:,:) :: a
      INTEGER :: n,isPrintMatrix,numProcesses,myProcessID,ierr
      LOGICAL :: isOK
      DOUBLE PRECISION :: runtime
      isOK = .TRUE.

      CALL MPI_Init(ierr)
      CALL MPI_Comm_size(MPI_COMM_WORLD, numProcesses,ierr)
      CALL MPI_Comm_rank(MPI_COMM_WORLD, myProcessID,ierr)

      !Get program input
      CALL GetUserInput(n,isPrintMatrix,isOK,numProcesses,myProcessID)

      ALLOCATE(a(n,n))
      
      IF (isOK .eqv. .FALSE.) GOTO 200

      !Master part
	  IF (myProcessID == 0) THEN

           ! Initialize the value of matrix a[n x n]
           CALL InitializeMatrix(a,n)

           ! Prints the input maxtrix if needed
           IF (isPrintMatrix == 1) THEN
                PRINT *,"Input matrix:"           
                CALL PrintMatrix(a, n)
           ENDIF
           
           ! Get start time
           runtime = MPI_Wtime()
      ENDIF

      ! Compute the Gaussian Elimination for matrix a[n x n]
      CALL ComputeGaussianElimination(a,n,isOK,numProcesses,myProcessID)

      IF (myProcessID == 0) THEN
           runtime = MPI_Wtime() - runtime
           
           IF (isOK .EQV. .FALSE.) THEN 
                PRINT *,"The matrix is singular"
           ELSE
                IF (isPrintMatrix .EQ. 1) THEN
                     PRINT *,"Output matrix:"           
                     CALL PrintMatrix(a, n)
                ENDIF
                WRITE (*,'(A,F0.2,A)') "Gaussian Elimination runs in ",runtime, " seconds" 
           ENDIF
      ENDIF

200   CALL MPI_Finalize(ierr)

      DEALLOCATE(a)

      STOP
      END
!----------------------------------------------------------------

