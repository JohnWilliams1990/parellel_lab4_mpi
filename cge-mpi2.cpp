//-----------------------------------------------------------------------
// Parallel Gaussian Elimination - C++ MPI Version 2
//-----------------------------------------------------------------------
//  Some features:
//  + For simplicity, program requires matrix size must be multiple 
//    of processes. 
//  + Data partition model of each process is block
//  + Data communication: Scatter / Gather / Broadcast
//  Programming by Gita Alaghband, Lan Vu 
//  Update in 8/17/2011
// ----------------------------------------------------------------------
#include <iostream>
#include <iomanip>
#include <cmath>
#include <stdio.h>
#include <stdlib.h> 
#include <mpi.h>    
using namespace std;
//-----------------------------------------------------------------------
//   Get user input of matrix dimension and printing option
//-----------------------------------------------------------------------
bool GetUserInput(int argc, char *argv[],int& n,int& isPrint,int numProcesses, int myProcessID)
{
	bool isOK = true;
	
	if(argc < 2) 
	{
		if (myProcessID==0) 
		{
			cout << "Arguments:<X> [<Y>]" << endl;
			cout << "X : Matrix size [X x X]" << endl;
			cout << "Y = 1: print the input/output matrix if X < 10" << endl;
			cout << "Y <> 1 or missing: does not print the input/output matrix" << endl;
		}
		isOK = false;
	}
	else 
	{
		//get matrix size
		n = atoi(argv[1]);
		if (n <=0) 
		{
			if (myProcessID==0) cout << "Matrix size must be larger than 0" <<endl;
			isOK = false;
		}
		//check if matrix size is multiple of processes
		if ( ( n % numProcesses ) != 0 )
		{
			if (myProcessID==0) cout << "Matrix size must be multiple of the number of processes" <<endl;
			isOK = false;
		}

		//is print the input/output matrix
		if (argc >=3)
			isPrint = (atoi(argv[2])==1 && n <=9)?1:0;
		else
			isPrint = 0;
	}
	return isOK;
}
//-----------------------------------------------------------------------
//Initialize the value of matrix a[n x n]
//-----------------------------------------------------------------------
void InitializeMatrix(float** &a,int n)
{
	a = new float*[n]; 
	a[0] = new float[n*n];
	for (int i = 1; i < n; i++)	a[i] = a[i-1] + n;

	for (int j = 0 ; j < n ; j++)
	{	
		for (int i = 0 ; i < n ; i++)
		{
            if (i == j) 
              a[j][i] = (((float)i+1)*((float)i+1))/(float)2;	
            else
              a[j][i] = (((float)i+1)+((float)j+1))/(float)2;
		}
	}
}
//------------------------------------------------------------------
//delete matrix matrix a[n x n]
//------------------------------------------------------------------
void DeleteMatrix(float **a,int n)
{
	delete[] a[0];
	delete[] a; 
}
//------------------------------------------------------------------
//Print matrix	
//------------------------------------------------------------------
void PrintMatrix(float **a, int n) 
{
	for (int i = 0 ; i < n ; i++)
	{
		printf("Row %d: \t", i+1) ;
		for (int j = 0 ; j < n ; j++)
		{
			printf("%.2f\t", a[j][i]);
		}
		cout<<endl ;
	}
}
//------------------------------------------------------------------
//Compute the Gaussian Elimination for matrix a[n x n]
//------------------------------------------------------------------
bool ComputeGaussianElimination(float **a,int n,int numProcesses, int myProcessID)
{
	float pivot,max,temp;
	int indmax,i,j,lk,k,master;
	int nCols = n/numProcesses;
	float *tmp = new float[n];
	float **b = new float*[nCols]; //create local matrix
	b[0] = new float[n*nCols];
	for (int j = 1; j < nCols; j++)
        b[j] = b[j-1] + n;
	MPI_Status status;
	//cout << "myProcessID" << myProcessID << endl; 				// this makes the difference. DONT KNOW WHY
	//process 0 send the data to compute nodes
	MPI_Scatter(a[0],n*nCols,MPI_FLOAT,b[0],n*nCols,MPI_FLOAT,0,MPI_COMM_WORLD);

	//Perform rowwise elimination
	for (k = 0 ; k < n ; k++)
	{

		max = 0.0;
		indmax = k;
		//printf("process %d has max %f\n",myProcessID, max );

		//ID of master process
		master = k/nCols;
		
		//local k
		lk = k%nCols;

		//Only master process find the pivot row
		//Then broadcast it to all other processes

		if (myProcessID == master)
		{	
			//Find the pivot row
			for (int i = k ; i < n ; i++) 
			{	
				temp = abs(b[lk][i]);     
				if (temp > max) 
				{
				  max = temp;
				  indmax = i;
				}
			}
		}

		MPI_Bcast(&max,1,MPI_FLOAT,master,MPI_COMM_WORLD);
		MPI_Bcast(&indmax,1,MPI_INT,master,MPI_COMM_WORLD);


		//If matrix is singular set the flag & quit
		if (max == 0) return false;

        // Swap rows if necessary
        if (indmax != k && myProcessID >= master)
		{
			for (j = ((myProcessID > master)?0:lk) ; j < nCols ; j++)
			{	
				temp = b[j][indmax];
				b[j][indmax] = b[j][k];
				b[j][k] = temp;
			}
		}
		//Master 
		if (myProcessID == master)
		{
			pivot = -1.0/b[lk][k];
			for (i = k+1 ; i < n ; i++) tmp[i]= pivot*b[lk][i];
		}

		MPI_Bcast(tmp + k + 1 ,n - k - 1,MPI_FLOAT,master,MPI_COMM_WORLD);

		//Perform row reductions
		if (myProcessID >= master)
		{
            for (j = ((myProcessID > master)?0:lk) ; j < nCols; j++)
			{
				for (i = k+1; i < n; i++)
				{
					b[j][i] = b[j][i] + tmp[i]*b[j][k];
				}
			}
		}
	}

	//process 0 collects results from the worker processes
	MPI_Gather(b[0],n*nCols,MPI_FLOAT,a[0],n*nCols,MPI_FLOAT,0,MPI_COMM_WORLD);

	delete[] b[0]; 
	delete[] b; 
	delete[] tmp; 
	return true;
}
//------------------------------------------------------------------
// Main Program
//------------------------------------------------------------------
int main(int argc, char *argv[])
{
	float	**a;	
	int	n,isPrintMatrix,numProcesses,myProcessID;
	bool missing;	
	double runtime;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &numProcesses);
	MPI_Comm_rank(MPI_COMM_WORLD, &myProcessID);
	
	//Get program input
	if (GetUserInput(argc,argv,n,isPrintMatrix,numProcesses,myProcessID)==false)
	{
		MPI_Finalize();
		return 1;
	}

	//Master part
	if (myProcessID == 0) 
	{	
		//Initialize the value of matrix A[N][N]
		InitializeMatrix(a,n);

		//Prints the input maxtrix if needed
		if (isPrintMatrix==1)
		{
			printf("Input matrix:\n");
			PrintMatrix(a,n); 
		}

		//Get start time
		runtime = MPI_Wtime();
	}

	//Compute the Gaussian Elimination for matrix a[n x n]
	missing = ComputeGaussianElimination(a,n,numProcesses,myProcessID);

	//Master process gets end time and print results
	if (myProcessID == 0)
	{
		runtime = MPI_Wtime() - runtime;

		if (missing == true)
		{
			//Print result matrix
			if (isPrintMatrix==1)
			{
				printf("Output matrix:\n");
				PrintMatrix(a,n); 
			}
			printf("Gaussian Elimination runs in %.2f seconds \n", runtime);
 		}
		else
		{
			cout<< "The matrix is singular" << endl;
		}

		//All process delete matrix
		DeleteMatrix(a,n);	
	}

	MPI_Finalize();
	return 0;
}
